var isMongo=false;
var message = "File System"
function run(object){
    var defaultConfig = {
        mongo:false,
        scriptDirPath:object.scriptDirPath,
        strict:false,
        jsonPath:__dirname+"/lib/",
        jsonName:'data-seeder.json'
    }
    if(object.mongo){
        isMongo=object.mongo;
        message = "Mongo-Db"
    }
    if(object.jsonPath){
        defaultConfig.jsonPath=object.jsonPath;
    }
    if(object.strict){
        defaultConfig.strict=object.strict;
    }
    console.log(`Data-Seeder Using ${message}`)
    return new Promise(
            function (resolve, reject) {
                try{
                    if (isMongo) {
                        require('./lib/dataSeederUsingMongo').seeder(defaultConfig).then(()=> {
                            resolve();
                        });
                    }
                    else {

                        require('./lib/dataSeederUsingFs').seeder(defaultConfig).then(function(){
                                resolve();
                        });


                    }
                }
                catch(e){
                    reject(e.stack)
                }
            })
}

module.exports={
    run:run
}