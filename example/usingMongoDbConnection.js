/**
 * Created by Suhas on 8/3/2016.
 */
var express= require('express');
var router = express.Router();
var app = new express();
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/sampleDataSeeder');
var dataSeeder = require("data-seeder");
var dataSeederPromise = dataSeeder.run({scriptDirPath:__dirname+'/scripts/',mongo:true});
dataSeederPromise.then(()=>{
        app.use(router);
        app.listen(3000,function(){
                console.log('listening on port 3000')
        })
        router.route('/')
                .get(function(req,res){
                        res.send("Sample Programme Running");
                })
})