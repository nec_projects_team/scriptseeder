# README #

Better way to execute set of scripts required for an application start.

##Prerequisites 

|Software|Version|
|---|---|
|nodejs|v 4.4.7|

## Install

```console
$ npm install data-seeder
```

## How to use

```js

var mongoose = require('mongoose');

mongoose.connect(<mongoDb-connection url>)

var data-seeder= require('data-seeder');
//
//<scriptDirPath> required parameters , is path of the directory which contains scripts to be executed
//
//<mongo> optional parameters . if set true data-seeder will use mongoose connection and use it to maintain file hash and executes the file which are updated, default mongo will be set to false
//<jsonPath> optional parameters . by default the hash files will registered into json file inside node_module/data-seeder/lib folder
//, if needed specific path for the json file to bes save it can passed here ex jsonPath:__dirname
var data-seederPromise= dataSeeder.run({scriptDirPath:<directoryPath>,jsonPath:<jsonPath>,mongo:<true or false>});

  data-seederPromise.then(()=>{

     //start your application from here once promise is returned.

})

.catch(error => {

  console.warn(error.stack)

  });
```

## Structure Of Script To Be Executed

```js

//in your script file you should export a function by the name run which will be executed by our module

//run method should should take callback as parameter and you should return the calback once your function executes completely.

module.exports={

    run:function(callback){

        //function to be executed

    }

}


```