/**
 * Created by zendynamix on 7/19/2016.
 */

var fs = require('fs');
var md5File = require('md5-file');
var mongoose = require('mongoose'),
        Schema = mongoose.Schema;
var seedDataSchema = new Schema({
    fileName:String,
    hash:String,
    timeStamp:Date
},{collection: "seedData"})

var seedData = mongoose.model('seedDataModel',seedDataSchema);
var scriptDirPath = ''
function seeder(config){
    try{
        return new Promise(
            function (resolve, reject) {
                scriptDirPath = config.scriptDirPath;
                mongoDbMethods.getDocsCount()
                .then((seedDataCollectionExist)=>{
                    if(seedDataCollectionExist){
                        checkForChangesInScriptDirectory()
                        .then(()=>{
                            resolve();
                        })
                    }else{
                        executeAllScriptsInDirectory().then(()=>{
                            resolve();
                        })
                    }
                }).catch(error => {reject(error.stack)});
            });
    }catch(err){
        console.log(err.stack)
    }
}
function checkForChangesInScriptDirectory(){
    return new Promise((resolve,reject)=>{
        fileSystemMethods.getAllFilesInScriptDirectory(scriptDirPath).then((files)=>{
            var checkForChangesInScriptDirectoryCounter = 0;
            files.forEach((file)=>{
                fileSystemMethods.createHash(file).then((hash)=>{
                    mongoDbMethods.getDocByName(file).then((doc)=>{
                        if(doc){
                            if(hash===doc.hash){
                                checkForChangesInScriptDirectoryCounter++;
                                console.log(`No Change Found in ${file} File`);
                                if(checkForChangesInScriptDirectoryCounter==files.length){
                                    resolve();
                                }
                            }
                            else{
                                doc.hash=hash;
                                mongoDbMethods.updateDoc(doc);
                                fileSystemMethods.executeRunMethodInScript(scriptDirPath+"/"+file).then(()=>{
                                    checkForChangesInScriptDirectoryCounter++;
                                    if(checkForChangesInScriptDirectoryCounter==files.length){
                                        resolve();
                                    }
                                })
                            }
                        }else{
                            fileSystemMethods.createHash(file).then((hash)=>{
                                var seedDataObj= new seedData();
                                seedDataObj.fileName=file;
                                seedDataObj.hash=hash;
                                mongoDbMethods.add(seedDataObj);
                                fileSystemMethods.executeRunMethodInScript(scriptDirPath+"/"+file).then(()=>{
                                    checkForChangesInScriptDirectoryCounter++;
                                    if(checkForChangesInScriptDirectoryCounter==files.length){
                                        resolve();
                                    }
                                })
                            })
                        }
                    })
                })
            })
        }).catch(error => {reject(error.stack)});
    })
}
function executeAllScriptsInDirectory(){
    return new Promise((resolve,reject)=>{
        fileSystemMethods.getAllFilesInScriptDirectory(scriptDirPath).then((files)=>{
            var executeAllScriptsInDirectoryCounter= 0;
            files.forEach((file)=>{
                fileSystemMethods.createHash(file).then((hash)=>{
                    var seedDataObj= new seedData();
                    seedDataObj.fileName=file;
                    seedDataObj.hash=hash;
                    mongoDbMethods.add(seedDataObj);
                    fileSystemMethods.executeRunMethodInScript(scriptDirPath+"/"+file).then(()=>{
                        executeAllScriptsInDirectoryCounter++;
                        if(executeAllScriptsInDirectoryCounter==files.length){
                            resolve();
                        }
                    })
                })
            })
        }).catch(error => {reject(error.stack)});
    })
}


var mongoDbMethods = {
    isSeedDataCollectionExist:()=>{
        return new Promise((resolve,reject)=>{
            mongoose.listCollections({name: 'mycollectionname'})
                .next(function(err, exist) {
                    if(err){reject(err.stack)}
                    if (exist) {
                       resolve(true)
                    }else{
                        resolve(false)
                    }
                });
        })
    },
    add:(seedData)=>{
        return new Promise((resolve,reject)=>{
            seedData.save((err,response)=>{
                console.info(`New Data For File ${seedData.fileName} added`)
                resolve();
            })
        })
    },
    getDocByName:(fileName)=>{
        return new Promise((resolve,reject)=>{
            seedData.findOne({'fileName':fileName},(err,response)=>{
                if(err){reject(err.stack)}else{resolve(response)}
            })
        })
    },
    updateDoc:(data)=>{
        return new Promise((resolve,reject)=>{
            data.save((err,response)=>{
                if(err){reject(err.stack)}
                else{
                    console.info(` data Updated ${response.fileName}`)
                    resolve()
                }
            })
        })
    },
    getDocsCount:()=>{
        return new Promise((resolve,reject)=>{
            seedData.count({},(err,count)=>{
                if(err){reject(err.stack)}if(err){reject(err);}else if(count>0){resolve(true);}else{resolve(false);}
            })
        })
    }
}
var fileSystemMethods = {
    getAllFilesInScriptDirectory:(scriptDirectoryPath)=>{
        return new Promise((resolve,reject)=>{
            fs.readdir(scriptDirectoryPath, function (err, files) {
                if(err){reject(err.stack)}
                else{resolve(files)}
            })
        })
    },
    createHash:(filePath)=>{
        return new Promise((resolve,reject)=>{
            md5File(scriptDirPath+"/"+filePath, (err, hash) =>{
                if(err){reject(err.stack)}
                else{resolve(hash)}
            })
        })
    },
    executeRunMethodInScript:(filePath)=>{
        return new Promise((resolve,reject)=>{
            require(filePath).run(function(){
                var fileName1 = filePath.split('/');
                console.log('Executed '+fileName1[fileName1.length-1]);
                resolve()
            })
        })
    }
}
module.exports={
    seeder:seeder
}