/**
 * Created by Suhas on 8/1/2016.
 */
var fs = require('fs');
var directoryPath = '';
var jsonFilePath = '';
var md5File = require('md5-file');
var configDetails=''
function seeder(defaultConfig) {
    configDetails = defaultConfig;
    return new Promise((resolve, reject)=> {
            directoryPath = fs.realpathSync(defaultConfig.scriptDirPath);
        try{

            jsonFilePath = fs.realpathSync(defaultConfig.jsonPath);
        }
        catch(e){
            console.log(e.stack)
        }
            isDataSeederInfoFIleExist(()=> {
                resolve();
            })
        }
    );
}
function isDataSeederInfoFIleExist(callBack) {
    try {
        var stats = fs.statSync(jsonFilePath+"/"+configDetails.jsonName);
        checkFiles().then(()=> {
            callBack();
        });
    }
    catch (e) {
        executeAllTheFilesInDirectory(callBack)
    }
}
function executeAllTheFilesInDirectory(callBack) {
    writeFileInfo().then(()=> {
        callBack();
    });
}
var json = {
    add:(objArray)=> {
        var dataArray = [];
        dataArray.push(objArray);
        fs.writeFile(jsonFilePath+"/"+configDetails.jsonName, JSON.stringify(dataArray), function (err) {
            if (err) {
                console.log(err.stack);
            } else {
                /*console.log("fileSaved");*/
            }
        });
    },
    listAll: ()=> {
        return new Promise((resolve, reject)=> {
            fs.readFile(jsonFilePath+"/"+configDetails.jsonName, (err, data)=> {
                if (err) {
                    reject(err);
                } else {
                    resolve(JSON.parse(data));
                }
            });
        })
    }
};
var fsMethods = {
    createHash: (filePath)=> {
        return new Promise((resolve, reject)=> {
            md5File(filePath, (err, hash)=> {
                if (err) throw err
                resolve(hash);
            })
        })
    },
    executeScriptContent: (filepath)=> {
        return new Promise((resolve, reject)=> {
            require(filepath).run(()=> {
                var fileName1 = filepath.split('/');
                console.log('Executed '+fileName1[fileName1.length-1]);
                resolve();
            });
        });
    },
    getAllFilesInProvidedDirectory: (directoryPath)=> {
        return new Promise((resolve, reject)=> {
            fs.readdir(directoryPath, function (err, files) {
                if (err) {
                    reject(err.stack)
                }
                else {
                    resolve(files)
                }
            });
        })
    },
}
function checkFiles() {
    return new Promise((resolve, reject)=> {
        json.listAll().then((data)=> {
            var existingFileDetails = data[0];
            var fileCheckerCounter = 0;
            fsMethods.getAllFilesInProvidedDirectory(directoryPath)
                .then((files)=> {
                    files.forEach((fileNameInDirectory)=> {
                        if (existingFileDetails[fileNameInDirectory.split(".")[0]]) {
                            fsMethods.createHash(directoryPath + "/" + fileNameInDirectory)
                                .then((hash)=> {
                                    if (existingFileDetails[fileNameInDirectory.split(".")[0]].hash === hash) {
                                        fileCheckerCounter++;
                                        console.log(`No Change Found in ${fileNameInDirectory} File`);
                                        if (fileCheckerCounter == files.length) {
                                            json.add(existingFileDetails);
                                            resolve();
                                        }
                                    }
                                    else if (existingFileDetails[fileNameInDirectory.split(".")[0]].hash != hash) {
                                        fileCheckerCounter++;
                                        existingFileDetails[fileNameInDirectory.split(".")[0]] = {
                                            fileName: fileNameInDirectory,
                                            hash: hash,
                                            lastModified: new Date()
                                        };
                                        fsMethods.executeScriptContent(directoryPath + "/" + fileNameInDirectory)
                                            .then(()=> {
                                                if (fileCheckerCounter == files.length) {
                                                    json.add(existingFileDetails);
                                                    resolve();
                                                }
                                            })
                                    }

                                })
                        }
                        else {
                            fsMethods.createHash(directoryPath + "/" + fileNameInDirectory)
                                .then((hash)=> {
                                    fileCheckerCounter++;
                                    existingFileDetails[fileNameInDirectory.split(".")[0]] = {
                                        fileName: fileNameInDirectory,
                                        hash: hash,
                                        lastModified: new Date()
                                    };
                                    fsMethods.executeScriptContent(directoryPath + "/" + fileNameInDirectory)
                                        .then(()=> {
                                            if (fileCheckerCounter == files.length) {
                                                json.add(existingFileDetails);
                                                resolve();
                                            }
                                        })
                                })
                        }
                    })
                })
        });
    });

}
function writeFileInfo(){
    return new Promise((resolve, reject)=> {
        fsMethods.getAllFilesInProvidedDirectory(directoryPath)
            .then((files)=> {
                var writFileChecker = 0;
                var dataObject = {};
                files.forEach((file)=> {
                    fsMethods.createHash(directoryPath + "/" + file)
                        .then((hash)=> {
                            dataObject[file.split(".")[0]] = {
                                fileName: file,
                                hash: hash,
                                lastModified: new Date()
                            }
                            fsMethods.executeScriptContent(directoryPath + "/" + file)
                                .then(()=> {
                                    writFileChecker++;
                                    if (writFileChecker == files.length) {
                                        json.add(dataObject);
                                        resolve();
                                    }
                                })
                        });
                })
            })
    })
}
module.exports = {
    seeder: seeder
}